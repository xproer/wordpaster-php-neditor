﻿/*
	版权所有 2009-2023 荆门泽优软件有限公司 保留所有版权。
	产品：http://www.ncmem.com/webapp/wordpaster/index.aspx
    控件：http://www.ncmem.com/webapp/wordpaster/pack.aspx
    示例：http://www.ncmem.com/webapp/wordpaster/versions.aspx
    版本：2.4.7
    时间：2023-07-12
    更新记录：
    		2021-01-18 优化代码
		2020-05-12 优化UE插件按钮注册方式。
        2012-07-04 增加对IE9的支持。
*/

function WordPasterManager()
{
    //url=>res/
    //http://localhost:8888/WordPaster/js/w.js=>
    this.getJsDir = function () {
        var js = document.scripts;
        var jsPath;
        for (var i = 0; i < js.length; i++) {
            if (js[i].src.lastIndexOf("WordPaster/js/w.layer.js") > -1) {
                jsPath = js[i].src.substring(0, js[i].src.indexOf("WordPaster/js/w.layer.js"));
            }
        }
        return jsPath;
    };
    var rootDir = this.getJsDir()+"WordPaster/";//WordPaster
    //http://localhost/WordPaster/css/
    var pathRes = rootDir + "css/";
    var _this = this;

    UE.registerUI('wordpaster', function (editor, uiName) {
        editor.registerCommand(uiName, {
            execCommand: function () {
                editor.focus();
                _this.PasteManual();
            }
        });
        var iconUrl = pathRes + 'word.png';

        var btn = new UE.ui.Button({
            name: "wordpaster",
            //提示
            title: 'Word一键粘贴',
            onclick: function () {
                editor.focus();
                _this.PasteManual();
            },
            getHtmlTpl: function () {
                return (
                    '<div id="##" class="edui-box %%">' +
                    '<div id="##_state" stateful>' +
                    '<div class="%%-wrap"><div id="##_body" unselectable="on" ' +
                    (this.title ? 'title="' + this.title + '"' : "") +
                    ' class="%%-body" onmousedown="return $$._onMouseDown(event, this);" onclick="return $$._onClick(event, this);">' +
                    ('<div class="edui-box edui-icon"><svg t="1603963671142" class="edui-iconfont" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1804" width="16" height="16"><path d="M534.3232 0.4096h69.4272v95.232c124.3136 1.024 248.6272-1.2288 372.736 1.024 12.4928-1.4336 24.7808 2.8672 33.5872 11.6736 8.8064 8.8064 12.9024 21.2992 11.264 33.792 2.048 234.2912 0 468.992 1.2288 703.0784-1.2288 23.9616 2.2528 50.5856-11.264 72.0896-16.9984 12.0832-38.912 10.8544-58.7776 12.0832H603.9552v93.3888h-72.0896C354.5088 990.208 177.3568 959.8976 0 928.1536V95.8464C177.9712 63.8976 356.1472 32.768 534.3232 0.4096z m0 0" fill="#2A528F" p-id="1805"></path><path d="M603.9552 131.2768h383.3856v761.0368H603.9552v-95.232h302.08v-48.128H603.9552V690.176h302.08v-48.128H603.9552v-58.9824h302.08v-48.128H603.9552v-60.2112h302.08v-46.2848H603.9552v-60.2112h302.08V321.536H603.9552v-59.5968h302.08v-47.5136H603.9552V131.2768zM240.4352 341.4016c22.1184-1.2288 44.2368-2.2528 66.3552-3.4816 15.5648 80.2816 31.3344 160.3584 48.128 240.64 13.1072-82.5344 27.648-164.864 41.7792-247.1936 23.1424-0.8192 46.4896-2.2528 69.632-3.6864-26.4192 115.3024-49.3568 231.6288-78.0288 346.112-19.456 10.4448-48.128 0-71.4752 1.2288-16.1792-78.848-33.9968-157.2864-47.9232-236.1344-13.5168 76.8-31.3344 152.9856-46.6944 229.1712-22.3232-1.2288-44.6464-2.4576-67.1744-4.096-19.456-104.8576-42.1888-208.896-60.416-313.7536 19.8656-1.024 39.7312-1.8432 60.2112-2.4576 12.0832 75.776 25.6 151.1424 36.0448 227.1232 16.1792-78.0288 32.768-155.648 49.5616-233.472z m0 0" fill="#FFFFFF" p-id="1806"></path></svg></div>') +
                    "</div>" +
                    "</div>" +
                    "</div></div>"
                );
            }
        });
        editor.addListener('selectionchange', function () {
            var state = editor.queryCommandState(uiName);
            if (state == -1) {
                btn.setDisabled(true);
                btn.setChecked(false);
            } else {
                btn.setDisabled(false);
                btn.setChecked(state);
            }
        });
        editor.addListener('ready', function () {
            _this.SetEditor(editor);
        });
        return btn;
    });
    UE.registerUI('importwordtoimg', function (editor, uiName) {
        editor.registerCommand(uiName, {
            execCommand: function () {
                editor.focus();
                _this.importWordToImg();
            }
        });
        var iconUrl = pathRes + 'word1.png';

        var btn = new UE.ui.Button({
            name: "importwordtoimg",
            //提示
            title: 'Word转图片',
            onclick: function () {
                editor.focus();
                _this.importWordToImg();
            },
            getHtmlTpl: function () {
                return (
                    '<div id="##" class="edui-box %%">' +
                    '<div id="##_state" stateful>' +
                    '<div class="%%-wrap"><div id="##_body" unselectable="on" ' +
                    (this.title ? 'title="' + this.title + '"' : "") +
                    ' class="%%-body" onmousedown="return $$._onMouseDown(event, this);" onclick="return $$._onClick(event, this);">' +
                    ('<div class="edui-box edui-icon"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">  <image id="image0" width="16" height="16" x="0" y="0" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAI6UExURQAAAAIFCQgQHA8eNRcvUx8/cCZOiy1cpBMqTBkzWx9AciVMhylUlSRGeBwxUR0yUh0yUx0zVBUoQ56z0Zmuz5yw0Jmvz5qw0Zaszs2KNhw5ZSJFeydQjipWmf+TAgQIDwoUIxEjPhkzXBdAfY5sRPyTA/+PAP+QAP+TAMlxACtYnSxZnytZnitXmytYnixboixaoCpXmypWmR5Nk5Orz/H3/+nx/YumzylVmSRSl1t9sYihxoegxoWexfDz+CtXmiRRliNRlidUmCBOlShUmCxXmiFPlXiUv7/M4b7L4PT3+iNQlmGCtGKCtE1yqqi61ixYm7bF3B1Mk1l7sISdxIOdxIKcxIGbw+rv9n2YwbHB2v///1R4ruPp8kVsph1Lk3OQvbXE3LTE2/P2+lF1rMLP4qi61eHo8Zqvz8nU5ihVmR1OmWWIvJ+215+1156115201u/1/TNentbf7MbS5Jaszerv9aW41RlLlzZWinKEoqCpuZ+ouJ2nt/Dp4NXe68nV5ld6r3CTxn1mUPGQDPOHAOiCAOeDA+eDAvyWDmWFtWKDtDNdnqa51j1pq8uAIP+SANCTR/iZHv+RAP+TAP+PACJPlSJQlR1NlSFSncuDJ/+OANWwgf2rO/KGBuyrV/CwXPKNDf2PAClWmiRUns2DJPGPCu6PFOOgSeiOGvKOD+6vXf2TDCxboStYnCpXmidVnLp9MrR5MOOgRv+/af+0Tv+nMPuoPStaoSZXosaCLP6TAf+QAP+YEXS8mSEAAAAqdFJOUwAJJ09/sN79c5C22fO6dnh5eGX+/v7+/v7+oMXl+/4SM1yMu+T9/v79vZbJrQEAAAABYktHRFt0vJU0AAAAB3RJTUUH6AoXCBkoERsUkgAAAkh6VFh0UmF3IHByb2ZpbGUgdHlwZSB4bXAAADiNlVVLsuMgDNxzijkC1heO48RmN1WznONPS8TJe86n3oRKbEBSt1qClL+//5Rf8fGmha88vHm1xdgupi5UjUzNrdvOG9E+LpfLIMJ6N4kVdVbZuMrmVRi2zXqR5qvDUdlX2VUMTwRkhhMRD96p8tUbr94MjrYFmC1UY25X251jrwQC2IiN4MHr3LibJ5NHGKxdwkPuHlS1yaa1UJAbnkvcaeeFNvCpGEBln2sMIyZu3DE8Bl2xSxxfpsG1sOFBrLTFEq+MHPELV6qnQbc0CWyMVyURsSPFcstxbkaazQWj8oq0hueHdocR7cncAbwksx5MmJIIFdpmEDzZUadQxhvSAwD2TyxAASVDQch6KtahFCycyjSA3AQ8CByspsBfaxJCP/NNsH2WqqTyO3po2IZUGvjUIA+J61Gzc0jZ0EWnsOV73M9hEXB7EZxdFKnpK5F/Hjya3KEXAtFQSglDI4KDxhzy84Ed4SUEVgSRqJ0GwLDroznLrTsrzstII863mu0GJih7paZ58sIB3cDUHxCp1yo9+yhKbkFepUvWbRohoZ4HdRXKdhW0K8jgQDY0I97RUYvEAW0FUzSxwAodjV1stTQWuC5PyJESu8ZZjJ57AJcbsjwhW563J+R3wOVAPq6Lb+iaHWxAxwTvUbc6y53VXkO5KU35L23kPcPyQZuvyG+1OaQpL5H159ocwDgiUx0an0/42WreRvdVXLWnu3huvfhD0GjITIfmXV7+AZ0lboe7sRpvAAABD0lEQVQY02NgYGBkYmZhZWPnYIAATi5uHi1tHV09Xj5+AQFBIQZ9Ay1DIyAwNjEFAjNzBiMwsDCytLK2sbG1E2awd3B0crZwcXVz9/Dw8PQSYfD28fXzDwi0CgoOCQ0LjxBlCIqMCo2OiY2LTwgMTExKFmNwTElNS8/IzMrOyc3LLygUZ3AqKi4pLSuvqKyqrqmprZNgMHKrb2iMbmpuaW1r7+jolGQwsuzq7unt658wcdLkKVOmTmUwsp823WLGzFmz58ydN3/BwkUQhy1esnTZ8tkrVq5avYZhrfa69UaLN2zctHnL1qmzt21nkJKWkdXesXPX7j179+zZM1sO6F95BUUlZRXVPWrq6uoamgDJOFfYv4O5pQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyNC0xMC0yM1QwODoyNTo0MCswMDowMKiiRZMAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjQtMTAtMjNUMDg6MjU6NDArMDA6MDDZ//0vAAAAKHRFWHRkYXRlOnRpbWVzdGFtcAAyMDI0LTEwLTIzVDA4OjI1OjQwKzAwOjAwjurc8AAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAAASUVORK5CYII=" /></svg></div>') +
                    "</div>" +
                    "</div>" +
                    "</div></div>"
                );
            }
        });
        editor.addListener('selectionchange', function () {
            var state = editor.queryCommandState(uiName);
            if (state == -1) {
                btn.setDisabled(true);
                btn.setChecked(false);
            } else {
                btn.setDisabled(false);
                btn.setChecked(state);
            }
        });
        editor.addListener('ready', function () {
            _this.SetEditor(editor);
        });
        return btn;
    });
    UE.registerUI('netpaster', function (editor, uiName) {
        editor.registerCommand(uiName, {
            execCommand: function () {
                editor.focus();
                _this.UploadNetImg();
            }
        });
        var iconUrl = pathRes + 'net.png';

        var btn = new UE.ui.Button({
            name: "自动上传网络图片",
            title: '自动上传网络图片',            
            onclick: function () {
                editor.focus();
                _this.UploadNetImg();
            },
            getHtmlTpl: function () {
                return (
                    '<div id="##" class="edui-box %%">' +
                    '<div id="##_state" stateful>' +
                    '<div class="%%-wrap"><div id="##_body" unselectable="on" ' +
                    (this.title ? 'title="' + this.title + '"' : "") +
                    ' class="%%-body" onmousedown="return $$._onMouseDown(event, this);" onclick="return $$._onClick(event, this);">' +
                    ('<div class="edui-box edui-icon"><svg t="1604542249252" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3538" width="16" height="16"><path d="M981.80396 245.351434h-103.532606l6.330181-5.064404-6.138828-8.036848c-88.171313-115.428848-221.743838-181.630707-366.466586-181.630707-144.104727 0-278.222869 66.176-367.967677 181.558303l-6.46206 8.308363 6.485333 4.865293H42.193455v534.630142h103.413656l-6.412929 4.809697 6.335353 8.292848C233.708606 908.511677 367.281131 974.713535 511.996121 974.713535c144.108606 0 278.229333-66.176 367.972849-181.559596l6.463353-8.309656-6.485333-4.864h101.858263V245.351434zM511.996121 102.456889c39.729131 0 81.978182 54.458182 111.418182 142.894545H400.535273c29.026263-88.435071 71.289535-142.894545 111.460848-142.894545z m167.262384 142.894545l-1.622626-5.222141c-15.472485-49.784242-35.569778-92.077253-58.61495-123.606626 78.681212 21.132929 149.761293 65.470061 204.735354 128.828767H679.258505zM405.33204 116.425697C382.38901 148.075313 362.686061 190.449778 347.880727 240.252121l-1.515313 5.099313H200.23596c55.058101-63.454384 126.271354-107.831596 205.09608-128.925737z m105.490101 806.442667c-0.372364-0.001293-0.744727-0.006465-1.118383-0.007758-39.511919-0.667152-81.418343-54.98699-110.677334-142.877737h222.873859c-28.934465 88.154505-71.021899 142.541576-111.078142 142.885495zM344.740202 779.982869l1.621333 5.215676c15.485414 49.825616 35.602101 92.148364 58.670546 123.686788-78.747152-21.103192-149.417374-65.471354-203.509657-128.902464h143.217778z m273.497212 129.039515c22.683152-31.464727 41.981414-73.412525 56.356202-124.070788l1.410586-4.97002H823.756283c-55.156364 63.569455-126.526061 107.993212-205.518869 129.040808z m-12.781899-418.160485l-70.093576 70.092283-132.395959-132.397253L118.89002 712.62901h-9.342707V312.705293h804.904081v399.923717h-87.228768L605.455515 490.861899z" fill="#31597C" p-id="3539"></path><path d="M769.00202 372.482586c-38.940444 0-70.09099 31.151838-70.09099 70.09099s31.150545 70.09099 70.09099 70.09099c38.939152 0 70.09099-31.151838 70.09099-70.09099s-31.151838-70.09099-70.09099-70.09099z m0 109.030141c-21.809131 0-38.940444-17.131313-38.940444-38.939151s17.131313-38.939152 38.940444-38.939152c21.807838 0 38.939152 17.131313 38.939152 38.939152s-17.131313 38.939152-38.939152 38.939151z" fill="#31597C" p-id="3540"></path></svg></div>') +
                    "</div>" +
                    "</div>" +
                    "</div></div>"
                );
            }
        });
        editor.addListener('selectionchange', function () {
            var state = editor.queryCommandState(uiName);
            if (state == -1) {
                btn.setDisabled(true);
                btn.setChecked(false);
            } else {
                btn.setDisabled(false);
                btn.setChecked(state);
            }
        });
        editor.addListener('ready', function () {
            _this.SetEditor(editor);
        });
        return btn;
    });
	UE.registerUI('wordimport', function (editor, uiName) {
        editor.registerCommand(uiName, {
            execCommand: function () {
                editor.focus();
                _this.importWord();
            }
        });
        var iconUrl = pathRes + 'word2.png';

        var btn = new UE.ui.Button({
            name: "wordimport",
            //提示
            title: '导入Word文档',
            onclick: function () {
                editor.focus();
                _this.importWord();
            },
            getHtmlTpl: function () {
                return (
                    '<div id="##" class="edui-box %%">' +
                    '<div id="##_state" stateful>' +
                    '<div class="%%-wrap"><div id="##_body" unselectable="on" ' +
                    (this.title ? 'title="' + this.title + '"' : "") +
                    ' class="%%-body" onmousedown="return $$._onMouseDown(event, this);" onclick="return $$._onClick(event, this);">' +
                    ('<div class="edui-box edui-icon"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">  <image id="image0" width="16" height="16" x="0" y="0" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAI3UExURQAAAP///wIFCQgQHA8eNRcvUx8/cCZOiy1cpBMqTBkzWx9AciVMhylUlSRGeBwxUR0yUh0yUx0zVBUoQ56z0Zmuz5yw0Jmvz6O205WrzJOpzKC00vP2+itXmq6/2ZOqzFJ2rU90q4Kcw+fs9JuwzxlJkYafxlF2rCxYmi9anERrppmuzkhuqHiUvzdhoMvW5tPc6z1lomSEtW+Nu1N3rYujyJiuziNRli9anG2MuVJ2rDVgn/Dz+fr7/T5moy5anE90q3aSvi1Zm4qix5GoyzJcnV1/shw5ZSJFeydQjipWmYafxUpwqVZ5rll7sDlioc7Y6AQIDwoUIxEjPhkzWyFCdiRNjDBbnaC00m2MuShVmF1+stXd6ytYnSxZnytZnitXmytYnixboixaoCpXmypWmR5Nk5Orz/H3/+nx/YumzylVmSRSl1t9sYihxoegxoWexfDz+CtXmiRRliNRlidUmCBOlShUmCxXmiFPlXiUv7/M4b7L4PT3+iNQlmGCtGKCtE1yqqi61ixYm7bF3B1Mk1l7sISdxIOdxIKcxIGbw+rv9n2YwbHB2v///1R4ruPp8kVsph1Lk3OQvbjG3bfG3bXE3PP2+lF1rMLP4qi61eHo8Zqvz8nU5h9OlWSEtTNentbf7MbS5Jaszerv9aW41BtKktXe68nV5ld6r3mVvxRFj2WFtWKDtDNdnqe61T9noxpJkiJPlSJQlR5NlBxMkyVSlyxboStYnCpXmhxLkydVmiZSl0hNcYsAAABddFJOUwAACSdPf7De/XOQttnzunZ4eXhl/v7+/sejosT+8ICUzNCfgef9gcn9+td0yZrqQDXktqLNjX3++6HO8hQI6PrFnvyNhPe/oMXl+4LXzMjqMxIzXIy75edkrP29NE8ogFEAAAABYktHRAH/Ai3eAAAAB3RJTUUH6QMHCBAgWFfBogAAArh6VFh0UmF3IHByb2ZpbGUgdHlwZSB4bXAAADiNlVVd0iMhCHz3FHsEBQQ9ziQzvm3VPu7xt1uTbJL5fuqLlYxBhAYaJv39/Sf94qdFT3rVES2yF1e/eA2T7OLVw7sfuosc43K5DBHIuxslNbTartn2yKbQbd6TtdgCF6vGZkc1xxMGVXFJRIcekvUaTbdojou+05kXyfzvVz9CeZboAWjMB3Hotg4e6hPJfzOQXXjDHjck12Z7zUkIbsQUaZdDi+zAk7HgVWPJFEoq2rRjBZdccSrKr8rQnNTxEK2yU6SbIkb84qrktyW3MAVoXLcqZub3ENMtxnXIMFsYVtYNYY2YHzkCSnJM5AHHZSLrRKIygUiSfRnBUwN1YmaiITw4wPkbCkBAyVAQ8T4z1pEpaISkpYB0C/whwUS1EvxcEybadpT8hHk6PLykR/YP8Gj4jnAaMGUGgDTjCYP7vXpPxjWskm+3hKSPrH9vnIwMBAedQXOhaQYkuFCXfeblo8A+D2q5TV/5vZu85WczVq2v5oGBq9GERiV9WX6zbjPJ6xIM2+yqDTRE34GGGbt641oD/0lahwQ0wg6YFE0bOEYEOGg44LMzUCdJ3xE9IXgHkF4QFMN18LvOBTExWQEBi7GfG9GZzlaZp9Av0C9skax68kxiKqnpzF89hd7PjhN/fuL51TGckuUVhIRf1KIy+uHX04jIGFpj6ujc5dnzqDTMZWl1VRDnCT2p0h899ZraOnvK4RF/sCdJ8hOnN38kIX2TBXSiF4TI4VcxknZUs7DCqDXKgZRAxgQkAO1TyItMr3NAOHnTMZPkDcEnBHhq2rvSQhKn3Ex6ciJW5+ibVJx7Rw+AqIlCeCY/Kyszd0WD8J0fI2/JU+EIm5mUcZ5P6XlAvautYfqQnt4k62S9ztLL+6yylDPpsl5F6R9COZsefA9SfwAAAQ5JREFUGNNjYGBgYmZhZWPn4GSAAC5uHt7YuPiERD5+AUFBIWGGpOTYlFQgSEvPAILMLIZUMMhOzcnNy88vKBRhKCouKS3LLq+orKqurq6pFWWoq29obGpuyW1ta+/o7OoWY2jt6e3o658wcdLklilTp00XZyiZMXPW7Dlzs+fNl5CUkpaZw1C6YOGixUuWLpOVk1dQVFJWYUitXL5iZd8qVTX11Rqaq7W0GVJz1qxdt36Dju5GPX0Dw2VGDKlFmzZnb9libLLVlJHRzNwC6rBKS6tKaxtbu232DNvjduxMTd3m4LjLydnFdYsbg7uHp1dccuJub58yX786/wCgfwODgkNCw8IjIqP2RMcwAgARjF47LciI/wAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyNS0wMy0wN1QwODoxNjozMSswMDowMKfRn1MAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjUtMDMtMDdUMDg6MTY6MzErMDA6MDDWjCfvAAAAKHRFWHRkYXRlOnRpbWVzdGFtcAAyMDI1LTAzLTA3VDA4OjE2OjMyKzAwOjAwsHEcrQAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAAASUVORK5CYII=" /></svg></div>') +
                    "</div>" +
                    "</div>" +
                    "</div></div>"
                );
            }
        });
        editor.addListener('selectionchange', function () {
            var state = editor.queryCommandState(uiName);
            if (state == -1) {
                btn.setDisabled(true);
                btn.setChecked(false);
            } else {
                btn.setDisabled(false);
                btn.setChecked(state);
            }
        });
        editor.addListener('ready', function () {
            _this.SetEditor(editor);
        });
        return btn;
    });
	UE.registerUI('excelimport', function (editor, uiName) {
        editor.registerCommand(uiName, {
            execCommand: function () {
                editor.focus();
                _this.importExcel();
            }
        });
        var iconUrl = pathRes + 'xls.png';

        var btn = new UE.ui.Button({
            name: "excelimport",
            //提示
            title: '导入Excel文档',
            onclick: function () {
                editor.focus();
                _this.importExcel();
            },
            getHtmlTpl: function () {
                return (
                    '<div id="##" class="edui-box %%">' +
                    '<div id="##_state" stateful>' +
                    '<div class="%%-wrap"><div id="##_body" unselectable="on" ' +
                    (this.title ? 'title="' + this.title + '"' : "") +
                    ' class="%%-body" onmousedown="return $$._onMouseDown(event, this);" onclick="return $$._onClick(event, this);">' +
                    ('<div class="edui-box edui-icon"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">  <image id="image0" width="16" height="16" x="0" y="0" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAIuUExURf////n8+e727tHm0aLMoni1eEGVQJ7KnrnZuZfGlnGxcUWZRSmJKBZ/FY3Ajfr9+vj7+Pn8+fj7+Pr8+pXElG6vbiiJJzCNMDCNLy+MLziROJDCkOXx5abOpni1d9rr2rnZuRmAGBmAGBV+FBd/FjSPMxN9EpLDkr/cvnq2eV2mXKHLoYq/ih2DHF+nXsvjy5nHmbLVsgd3BpHCkNfp15jGl3GxcMbgxqrQqh6DHRB7DxiAF+Hv4WisZ9zs3DyTO5LDkcfhx4K7gl6mXavRqpLDkh6DHW6vbZbFlZLDkdTo1JrHmYa9hqzRq5DCkBqBGWmtaZfGlxV+FKPMooC5fzuTOzmSOHWzdM7kzjOPMh+EHuDu4F+nX+Xx5TmSOH+4fj2UPBJ8ERZ+FSqKKZXFlH24fcHdwa/TrrHUsDaQNWyuax+EHsTfxM7kziWHJFWiVSGEICCEHziRN2uuaj+VPn24fIvAiwl3CBV+FGisaD+WPx2DHO727vn8+SeIJkGWQGisZxR9E3y3e4S7gxmAGEydTKTNo4C5gFijWB2DHA56DXi1dzOOMkGWQEWYRB+EH9Hm0eLv4sLdwpPDkmarZVOgUq3SrVqkWQ56DUmaSM/lzwl3CAd2BgBvAAZ2BQJzAQJ0AQt4Cg56DRB7Dw16DAp4CQFzAA97Dg15DAR0Awh3BxF8EABxAAx5CwN0AgBuAAR1AwByAAx4CwNzAgV1BAt4CQJzAAl4CAZ1BP///yA8vyEAAACbdFJOUwAACCVMfLFaN12Js9fxcQMEBAUCZH/U1NPTy2wWUn8lPe3w8vHI+2stcZZPY+mWJF49/GweXIY1S+f+7BKMGMdrKGqSQ1jojWBqIF5vQFnsj2DuWonO0ZQtzecVkg7Kfs/9+uJmeDBGQMqa6kA15LXk5bmWzo19/vufzfIUCOjDnfyNhPe/T3eh5/yG2czI6jIUNF2Mo0uu/b00ftoRxAAAAAFiS0dEAIgFHUgAAAAHdElNRQfpAwcIEg62t67vAAACunpUWHRSYXcgcHJvZmlsZSB0eXBlIHhtcAAAOI2VVUGS3CAMvPOKPAFLQoLneGxzS1WOeX66Ycbxjnc3FbtmsAVIrVYLp98/f6UfvGq0pJv2qJF9cfWHlzDJLl48vPmhu8jRH49HF4G9udFSQovtmm2PbIq11VuyGmtgY9FY7SjmGOFQFZtEtOshWbeoukZ1bPSdwXyRzHff/AjlXGIEoDHvxKHrnDiXDyR/3cD24A47d0gu1faSkxBcj2HSJocusgNPxo2oGtOmWKSiVRvu4C0bZkX5U+makzoG0SI7TboqcsQ/tkp+u+WZpgCN61rEzPyVYnrmOCeZZg3DnXVFWj3GJUdgkRwDeSDwMpA1IlEZQCTJPp1g1ECdyExUpIcAmH9DAQgoGQoi3gZjDUxhRUiaC0C3IB4IJqpJ8LUmJNp2lPyGeQQ8fEkn+wd01H1HOhWYMhMAzRjhcH9V7+Jcwwr19iQkfeb9386pyEByWNPpLjSNhAQbyvRPXj5L7OukZtj0XdyXyyc/q7FqbTYPHGxGFxqF8mX5zZoNkucmOLbRVStkiL6DDDOeylNrFfqnaB0WyAhPwKRo2sA0MsBExQTHxkSdIn1HdEHwDiBdEcBPHm1g+FfFPmLC+6KLoZ9toZHlQmi+oY04mlhJ2FBvkSlMpTSd/JVb6u0emI70fyJ/DAxnVHmBIBEXtSjMvvt2OyIyDq0+1uh4yqPnUWn0XpZaZgUxn9CTKu3sqY/UltFTjoh4wTNFki+aXv0kIX3PArJ2X8ADD7+CI2lHNRdWGLUuSkpgAymeALQNIzeSXucB4dQNSBxsXBF8IYBL074WTSRx42bIkydicZZgSHE8o3IUaqIRkalPKJZA8bRoEL7zMuqWOhUeYYNJ6ffzKV0PqPdl8zA9rbcvyZyZn7P04XtWWMpBusxPUfoDo8+bRNTnrVIAAAEWSURBVBjTY2AAAiZmFlY2dgYI4ODk4ubhnT1nLh+/gKCQkDDDvPkL5ixctHjxAhFRMXEJSSmGxYsXLVq8ZOniZdIysnLyCooMi5crKa9QUVVbpq6hqaWto8uweJ6e/koDQ6NlxiamZuYWlgxWS6xtbO3sVy1zcHRydnF1Y7BavMyd0WPZ6jWeXt4+vn7+IIEAxsBlK4KCQ0LDwiMiGazWRkXHxMati09Yn5i0PjmFYfGG1LS56RmZWWuyc3LzNuYDrS1Q3FRYVFyyuZSBoay8AuiwJUtWr11TWbWluqa2bmsFw7ptG2avWL69vmFjY1Nzy4JWhrb2DtXOrh0Luns29Pat6p8A8jDjxEmTp0ydNn3GwpmzGABOvGJXwQk1vQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyNS0wMy0wN1QwODoxODoxNCswMDowMKkFhzoAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjUtMDMtMDdUMDg6MTg6MTQrMDA6MDDYWD+GAAAAKHRFWHRkYXRlOnRpbWVzdGFtcAAyMDI1LTAzLTA3VDA4OjE4OjE0KzAwOjAwj00eWQAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAAASUVORK5CYII=" /></svg></div>') +
                    "</div>" +
                    "</div>" +
                    "</div></div>"
                );
            }
        });
        editor.addListener('selectionchange', function () {
            var state = editor.queryCommandState(uiName);
            if (state == -1) {
                btn.setDisabled(true);
                btn.setChecked(false);
            } else {
                btn.setDisabled(false);
                btn.setChecked(state);
            }
        });
        editor.addListener('ready', function () {
            _this.SetEditor(editor);
        });
        return btn;
    });
    UE.registerUI('pptimport', function (editor, uiName) {
        editor.registerCommand(uiName, {
            execCommand: function () {
                editor.focus();
                _this.importPPT();
            }
        });
        var iconUrl = pathRes + 'ppt.png';

        var btn = new UE.ui.Button({
            name: "pptimport",
            //提示
            title: '导入PPT文档',
            onclick: function () {
                editor.focus();
                _this.importPPT();
            },
            getHtmlTpl: function () {
                return (
                    '<div id="##" class="edui-box %%">' +
                    '<div id="##_state" stateful>' +
                    '<div class="%%-wrap"><div id="##_body" unselectable="on" ' +
                    (this.title ? 'title="' + this.title + '"' : "") +
                    ' class="%%-body" onmousedown="return $$._onMouseDown(event, this);" onclick="return $$._onClick(event, this);">' +
                    ('<div class="edui-box edui-icon"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">  <image id="image0" width="16" height="16" x="0" y="0" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAH1UExURf////318/fe2PDAtOedi994YNdZPNA6F+mikeWRfd51XdhdQNRMLOB8Ze+6ru+6ru65rPLJv9pkSN95Ye+6rf///////9RNLeiejeWPfNtoTdpjRuOHcvro5OqpmdJDIeiejNxuVNNIJ9RKKthcP+ytntxqUOSOetZSM/XSyfbZ0tdWON96YuOKdNxrUOebiOqmltFBHtNKKeSLdttrUNVQMP3y8P/6+tdYOtNKKtxsUuOLdtNJKOeZh+igj9RNLd51XOKGcNtrUdZUNtJGJeiejdpjR9xsUtxvVdVSM/fb1fvu6vXVze20p+WSf91xV9VQMdVQMe20puGAadJFI91yWfba1NFCINE/HdFAHtJEItA8GdA+HNJEI9JGJdE/HOOJc/LHvfPMwvHEufHDuOKFb9FBH9JDIdA7GOupmfzw7fXSyv///+qomNNIJ9hcP9pnTNVRMtJHJtJFJNVQMeOLdueah+mkk9dZPPzv7fro5NlfQtJBH9FAH+WQfNliRdlgQ/zu69hfQvzu6vvr5+eaiNpnS91vVeeYhv/9/NheQfzv6+SNeP729OGBa9E+G9JDIv739fnj3tJFI9JCINleQfzy79haPc86Fs82Ec4zD9hZPPfc1dhZO883E885FdNHJtRPL9E9G9A6F9A+G9A8GtA9Gs9NpjQAAABXdFJOUwAJJk59r9z9dZC22fOiS0xMPsWgn7387XqTzM+ceOL8e8n9+tdrxJfqQDXktZ7MjX3++53N8hQI6PvBnPyNhPe/oMXl+3vWzMjqMhIzXIy75eRfrP29NISb1OkAAAABYktHRACIBR1IAAAAB3RJTUUH6QMHCBgyYzc64gAAArx6VFh0UmF3IHByb2ZpbGUgdHlwZSB4bXAAADiNlVVLsuMgDNxzijkCloQEx3Fis5uqWc7xpxucjBO/Tz27YhMBUqvVwunv7z/pF68aLelde9TIvrj6zUuYZBcvHt58101k77fbrYvA3txoKaHFNs22RTbF2uotWY01sLForLYXc7zhUBWbRLTrLlnvUXWN6tjoG4P5Ipn//e57KOcSIwCNeScOXefEc/lA8t8NbDfusOcOyaXaVnISgusxTNpk10U24Mm4EVVj2hSLVLRqwx285Y5ZUf5UuuakjpdokY0mXRU54omtkt9uOdIUoHFdi5iZP1JMR45zkmnWMNxZV6TVY1yyBxbJPpAHAi8DWSMSlQFEkmzTCd4aqBOZiYr0EADzbygAASVDQcTbYKyBKawISXMB6BbEA8FENQk+14RE24aSXzCPgLsv6cn+Dh1135BOBabMBEAz3nC4Pap3cq5hhXo7CEkfef/eORUZSA5rOt2FppGQYEOZ/snLR4l9ntQMm76K+3B58LMaq9Zm88DB3ehCo1C+LL9Zs0Hy3ATHNrpqhQzRd5BhxqgcWqvQP0XrsEBGGAGTomkD08gAExUTfDcm6hTpO6ITgncA6QVBRRS3BYpFVJTFickWjBdDPxsI1GboNGXzLOiERcXQBKoJZr1EpjDpZ3HyVy6pt2vgBJ/+k8ivgRGUKi8QJOKiFoXZd79fjoiMQ6uPNTpGefQ8Ko3ey1LLrCDmE3pSpT176pXaMnrKERF/MKZI8knTqz9JSN+wwPIjTbDhDhJQbBtn0Cg77AZkG9YEzqOhjwz/IEv5pBZ4ZBWfAjgj+EQAp6Z9LJpI4sLNkOfhHgUYUhxjRw9AqIlGlIL6pHZ0jKAhCtR5GXVLnQqPsMGk9Ov5lM4H1PuyeZg+rZcvyZyZn7P08j0rLOUgXeanKP0D5w+a5et6eLEAAAEGSURBVBjTY2BgYGRiZmFlY+dggABOLm6e8IjIqAhePn5+fgFBhuiY8Ng4IIhPSExKTklJZQCy0yLT4+IyMrOyc3JycoECefkFheFFxSWlGWU5OeUMceEVlVlV1TVxtXX1DY1ggabm6pbWjLj0tvaOTrBAV3dPb1//hHQhYRFRMZBA/sRJuZOniEtISknLyMoBBaZOmz5jsryC4kwl5Vkqqgxx6bPnzA2vUVOfp6GppT1fB2jtgoV5cYt09RbrMzAYGBqBHBYVF7fI2GSJqZm5xRRLhqUR6cVxcTVW1jNsbO3sMxwYHJ2cXSJiIqe4ui1z94j09AL619vH188/IDAoOCQ8NIwBALOcVBgBk5TsAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDI1LTAzLTA3VDA4OjI0OjQ5KzAwOjAwA+xuEQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyNS0wMy0wN1QwODoyNDo0OSswMDowMHKx1q0AAAAodEVYdGRhdGU6dGltZXN0YW1wADIwMjUtMDMtMDdUMDg6MjQ6NTArMDA6MDB8lrI/AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAABJRU5ErkJggg==" /></svg></div>') +
                    "</div>" +
                    "</div>" +
                    "</div></div>"
                );
            }
        });
        editor.addListener('selectionchange', function () {
            var state = editor.queryCommandState(uiName);
            if (state == -1) {
                btn.setDisabled(true);
                btn.setChecked(false);
            } else {
                btn.setDisabled(false);
                btn.setChecked(state);
            }
        });
        editor.addListener('ready', function () {
            _this.SetEditor(editor);
        });
        return btn;
    });
    UE.registerUI('pdfimport', function (editor, uiName) {
        editor.registerCommand(uiName, {
            execCommand: function () {
                editor.focus();
                _this.ImportPDF();
            }
        });
        var iconUrl = pathRes + 'pdf.png';

        var btn = new UE.ui.Button({
            name: "pdfimport",
            //提示
            title: '导入PDF文档',
            onclick: function () {
                editor.focus();
                _this.ImportPDF();
            },
            getHtmlTpl: function () {
                return (
                    '<div id="##" class="edui-box %%">' +
                    '<div id="##_state" stateful>' +
                    '<div class="%%-wrap"><div id="##_body" unselectable="on" ' +
                    (this.title ? 'title="' + this.title + '"' : "") +
                    ' class="%%-body" onmousedown="return $$._onMouseDown(event, this);" onclick="return $$._onClick(event, this);">' +
                    ('<div class="edui-box edui-icon"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" enable-background="new 0 0 16 16" xml:space="preserve">  <image id="image0" width="16" height="16" x="0" y="0" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAGVUExURf///+qipdM+RMcvNdOVmNM/RZsUGdiho5gOE9efodI7QZYMEdSdn7okK/PS0/PGx/PIydtgZeOBheODh9teY/TNztI4PueVmOSIjNtfZNpdYuJ8gPfZ2vPHyfG/wdteZNM9Q9RARtdOU/je3+qhpOeSldZITfXP0PbX2NdMUt9vdOJ9gdtgZeeVmOqho9E1O9M+ROSEiNtiZ9VFS/3x8v/6+tdOU9M/RdxjaOODh9M+ROeTl+iandRCSN5tcdM9Q9I4PuiWmdpbYNxjaNxma9VITvfZ2uibntM9Q9VHTOqgo+B3e9I6QN1qbvbY2dI6QNI7QdQ7QdI5P9Q8QsgzOck1O5sTGMs2PJwWG5cRFtM8QsEvNborMbkoLdRBR9U9Q9Y9Q9Y+RNQzOtEzOeynquumqdEzOtE2PNAyONAtNNExOO6ytdAuNNE3PdAxN+J/g/C6vNAvNemdoM8qMM8pMNEyOOF3e/PKy88tNO2vsfro6NdNU9M+ROqhpNxjaM8oL9AyOdI3PdE1O9I2PM8uNNAwNv///6ONvl8AAABPdFJOUwBw8ftr9Plf/mH9/mH+NEhGx6Gfwz7ufZTMz6Qw/X7J/frlLMaZ6kA15LuezY19/vuezfIUCOj7w538jYT3v/n7fdTMyOoyefPmcK39vTTPESy/AAAAAWJLR0QAiAUdSAAAAAd0SU1FB+kDBwgZClIusz0AAAK0elRYdFJhdyBwcm9maWxlIHR5cGUgeG1wAAA4jZVVS7LjIAzcc4o5ApaEBMdxYrObqlnO8acbEk9iv089UwkyCKkltXD6+/tP+sWnRkt61x41si+ufvMSJtnFi4c333UT2fvtdusiWG9uXCmhxTbNtkU2hW71lqzGGjhYNFbbizlmGFTFIRHtukvWe1RdozoO+kZnvkjmu999D+VeogegMe/EoevcONQHkv9msHbjCTtOSC7VtpKTEFyPsaRNdl1kA56MAa8ac02hpKJVG0ZwyB27ovypdM1JHZNokY1LuipixD+OSj4NeYQpQOO6FjEzf4aYHjHOTYZZwzCyrgirx3hkDyjJPpAHHC8DWSMSlQFEkmzTCGYN1ImZiYrw4AD7JxSAgJKhIOJtZKwhU9AISVMB6Rb4Q4KJaib4tSZMtG0o+QXzcLj7ko7s7+BR9w3hVGDKDABpxgyD27N6L8Y1rJBvj4Skj6x/b5yMDAQHnU5zoWkEJDhQpn3m5aPAPg9quk1f+X2afORnNVatzeaBgbvRhEYhfVl+s2YjyfMQDNvoqhU0RN+BhhlSeXCtgv8krWMFNIIETIqmDWwjAmxUbHBuDNRJ0jOiFwRnAOkNQQHN1MBtE0hYJibIiy6GfrYFiJrR5QJ5QSfMHzwnHMgXzySmkprO/JVL6O3qmIbKTzy/O4ZTsryAkPCLWhRG3/1+uSIyLq0+dHRIefQ8Ko3ey1LLrCD2E3pSpR099Z7aMnrK4REvkEmS/MLp1Y8kpG+ysIzSAw6LrmjmUVtEDcmhMaXK8ju5gNjzGLie/JmNwJ0kJwSfEOClaZ9KE0lccjPoyRuxOK++QcUhAwKJmrgIz+QnweuQFg0S1PkYeUueCq+wkUnp1/spvV5QZ7V5mR6rly/J3Jmfs/T2PSss5Ui6zE9R+gdSZpsQind95wAAAM9JREFUGNNjYGTyDwCBQGYWBjBgDQLzA4JD2NjBAhB5oEBoGAcnSIALJhAeEcnNAxcICooKiI6J5eWDCvjHxfkHxCckJvFDBZJTUtMC0jMyMgWgAlmp2Tm5gkLCIqJQgbz8AjFxCUkpaRmIgH9hkaycfLGCYomSMkjAv7SsXEW1WE1dQ7NCCyjgH1RZVa2tU6jLwKCnbwASqKmtqzc0ajA2MTVrNAc7Pa+p2cKyxcraxrbVjsEe4jkHR6cMZ5cmVzcGdw+w93I9vbx9cn39GADLVUEQo6JLlAAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAyNS0wMy0wN1QwODoyNTowOCswMDowMM4TAGEAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMjUtMDMtMDdUMDg6MjU6MDgrMDA6MDC/TrjdAAAAKHRFWHRkYXRlOnRpbWVzdGFtcAAyMDI1LTAzLTA3VDA4OjI1OjEwKzAwOjAwFx7X+wAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAAASUVORK5CYII=" /></svg></div>') +
                    "</div>" +
                    "</div>" +
                    "</div></div>"
                );
            }
        });
        editor.addListener('selectionchange', function () {
            var state = editor.queryCommandState(uiName);
            if (state == -1) {
                btn.setDisabled(true);
                btn.setChecked(false);
            } else {
                btn.setDisabled(false);
                btn.setChecked(state);
            }
        });
        editor.addListener('ready', function () {
            _this.SetEditor(editor);
        });
        return btn;
    });

    this.Editor = null;
    this.Fields = {}; //符加信息
    this.UploadDialogCreated = false;
    this.PasteDialogCreated = false;
    this.imgUploaderDlg = null;//jquery obj
    this.event={
        scriptReady: function () {
            $(function () {
                //加载
                if (typeof (_this.ui.render) == "undefined") {
                    _this.LoadTo($(document.body));
                }
                else if (typeof (_this.ui.render) == "string") {
                    _this.LoadTo($("#" + _this.ui.render));
                }
                else if (typeof (_this.ui.render) == "object") {
                    _this.LoadTo(_this.ui.render);
                }
            });
        },
        dataReady:function(e){
            //e.word
            //e.imgs
        }
    };
    this.ui = { setup: null,
        single: null,
        dialog: { files: 0/**word图片上传窗口 */, paste: 0/**粘贴窗口 */ },
        paste:{dlg:null,ico:null,msg:null,percent:null},
        ico: {
            error: pathRes + "error.png",
            upload: pathRes + "upload.gif"
        }
    };
    this.data={
        browser:{name:navigator.userAgent.toLowerCase(),ie:true,ie64:false,chrome:false,firefox:false,edge:false,arm64:false,mips64:false,platform:window.navigator.platform.toLowerCase()},
        error:{
        "0": "连接服务器错误",
        "1": "发送数据错误",
        "2": "接收数据错误",
        "3": "未设置文件路径",
        "4": "本地文件不存在",
        "5": "打开本地文件错误",
        "6": "不能读取本地文件",
        "7": "公司未授权",
        "8": "未设置IP",
        "9": "域名未授权",
        "10": "文件大小超出限制",
        "11": "不能设置回调函数",
        "12": "Native控件错误",
        "13": "Word图片数量超过限制"
      },
      type:{local:0/*本地图片*/,network:1/*网络图片*/,word:2/*word图片*/},
      scripts: [
          "css/w.css",
          "js/w.edge.js",
          "js/w.app.js",
          "js/w.file.js"
      ],
      jsCount: 0,//已经加载的脚本总数
    };
    
    this.loadScripts = function () {
        var head = document.getElementsByTagName('head')[0];
        //加载js
        for (var i = 0, l = this.data.scripts.length;
            i < l;
            ++i) {
            var n = this.data.scripts[i];
            if (-1 != n.lastIndexOf(".css")) {
                var css = document.createElement("link")
                css.setAttribute("rel", "stylesheet")
                css.setAttribute("type", "text/css")
                css.setAttribute("href", rootDir + n);
                head.appendChild(css);
            }
            else {
                this.requireJs(rootDir + n, function () {
                    _this.data.jsCount++;
                    if ((_this.data.jsCount + 1) == _this.data.scripts.length)
                        _this.event.scriptReady();
                });
            }
        }
    };
    this.requireJs = function (js, callback) {
        // create script element
        var head = document.getElementsByTagName('head')[0];

        var script = document.createElement("script");
        script.src = js;

        // monitor script loading
        // IE < 7, does not support onload
        if (callback) {
            script.onreadystatechange = function () {
                if (script.readyState === "loaded" || script.readyState === "complete") {
                    // no need to be notified again
                    script.onreadystatechange = null;
                    // notify user
                    callback();
                }
            };

            // other browsers
            script.onload = function () {
                callback();
            };
        }

        // append and execute script
        head.appendChild(script);
    };
    this.ffPaster = null;
    this.ieParser = null;
    this.ffPasterName = "ffPaster" + new Date().getTime();
    this.iePasterName = "iePaster" + new Date().getTime();
    this.setuped = false;//控件是否安装
    this.websocketInited = false;
    this.natInstalled = false;
    this.filesPanel = null;//jquery obj
    this.fileItem = null;//jquery obj
    this.line = null;//jquery obj
	this.Config = {
        "EncodeType"		    : "GB2312"
        , "Company"			    : "荆门泽优软件有限公司"
        , "Version"			    : "1,5,141,60875"
        , "License2"			: ""
        , "Debug"			    : false//调试模式
        , "LogFile"			    : "f:\\log.txt"//日志文件路径
        , "PasteWordType"	    : ""	//粘贴WORD的图片格式。JPG/PNG/GIF/BMP，推荐使用JPG格式，防止出现大图片。
        , "PasteImageType"	    : ""	//粘贴文件，剪帖板的图片格式，为空表示本地图片格式。JPG/PNG/GIF/BMP
        , "PasteImgSrc"		    : ""	//shape:优先使用源公式图片，img:使用word自动生成的图片
        , "JpgQuality"		    : "100"	//JPG质量。0~100
        , "PowerPoint"          : {sleep:500/*解析powerpoint时延迟时间*/}
        , "PDF"                 : {zoom:150/*缩放比例*/}
        , "QueueCount"		    : "5"	//同时上传线程数
        , "CryptoType"		    : "crc"//名称计算方式,md5,crc,sha1,uuid，其中uuid为随机名称
        , "ThumbWidth"		    : "0"	//缩略图宽度。0表示不使用缩略图
        , "ThumbHeight"		    : "0"	//缩略图高度。0表示不使用缩略图
        , "FileFieldName"		: "file"//自定义文件名称名称
        //图片地址匹配规则配置教程：http://www.ncmem.com/doc/view.aspx?id=07e3f323d22d4571ad213441ab8530d1
        , "ImageMatch"		    : ""//服务器返回数据匹配模式，正则表达式，提取括号中的地址
        //自定义图片地址配置教程：http://www.ncmem.com/doc/view.aspx?id=704cd302ebd346b486adf39cf4553936
        , "ImageUrl"		    : ""//自定义图片地址，格式"{url}"，{url}为固定变量，在此变量前后拼接图片路径，此变量的值为posturl返回的图片地址
        , "FileCountLimit"		: 300//图片数量限制
        , "AppPath"			    : ""
        , "Cookie"			    : ""
        , "Servers"             : [{"url":"www.ncmem.com"}]//内部服务器地址(不下载此地址中的图片)
        , "Proxy"               : {url: ""/**http://192.168.0.1:8888 */,pwd: ""/**admin:123456 */}//代理
        , "WebImg"              : {urlEncode:true/*下载外部图片地址是URL是否自动编码，默认情况下自动编码，部分网站URL没有进行编码*/}
        //上传接口配置教程：http://www.ncmem.com/doc/view.aspx?id=d88b60a2b0204af1ba62fa66288203ed
        , "PostUrl"			    : "http://www.ncmem.com/products/word-imagepaster/fckeditor2461/asp.net/upload.aspx"
        , "Fields"              : {}
        //x86
        ,ie:{name:"Xproer.WordParser2",clsid:"2404399F-F06B-477F-B407-B8A5385D2C5E",path:"http://res2.ncmem.com/download/WordPaster/fast/2.0.42/WordPaster.cab"}
        ,ie64:{name:"Xproer.WordParser2x64",clsid:"7C3DBFA4-DDE6-438A-BEEA-74920D90764B",path:"http://res2.ncmem.com/download/WordPaster/fast/2.0.42/WordPaster64.cab"}
        //Firefox
        , "XpiType"	            : "application/npWordPaster2"
        , "XpiPath"		        : "http://res2.ncmem.com/download/WordPaster/fast/2.0.42/WordPaster.xpi"
        //Chrome
        , "CrxName"		        : "npWordPaster2"
        , "CrxType"	            : "application/npWordPaster2"
        , "CrxPath"		        : "http://res2.ncmem.com/download/WordPaster/fast/2.0.42/WordPaster.crx"
        //Edge
        , edge: { protocol: "wordpaster", port: 17092, visible: false }
        , "ExePath": "http://res2.ncmem.com/download/WordPaster/pdf/1.0.23/WordPaster.exe"
        , "mac": { path: "http://res2.ncmem.com/download/WordPaster/mac/1.0.36/WordPaster.pkg" }
        , "linux": { path: "http://res2.ncmem.com/download/WordPaster/linux/1.0.25/com.ncmem.wordpaster_2020.12.3-1_amd64.deb" }
        , "arm64": { path: "http://res2.ncmem.com/download/WordPaster/arm64/1.0.22/com.ncmem.wordpaster_2020.12.3-1_arm64.deb" }
        , "mips64": { path: "http://res2.ncmem.com/download/WordPaster/mips64/1.0.19/com.ncmem.wordpaster_2020.12.3-1_mips64el.deb" }
    };

    if (arguments.length > 0) {
        var cfg = arguments[0];
        if (typeof (cfg) != "undefined") $.extend(true, this.Config, cfg);
        if (typeof (cfg.ui) != "undefined") $.extend(true, this.ui, cfg.ui);
        if (typeof (cfg.event) != "undefined") $.extend(true, this.event, cfg.event);
    }

	this.EditorContent = ""; //编辑器内容。当图片上传完后需要更新此变量值
	this.CurrentUploader = null; //当前上传项。
	this.UploaderList = new Object(); //上传项列表
    //已上传图片列表
    //模型：LocalUrl:ServerUrl
	this.UploaderListCount = 0; //上传项总数
	this.fileMap = new Object();//文件映射表。
	this.postType = this.data.type.word;//默认是word
    this.working = false;//正在上传中
    this.pluginInited = false;
    var browserName = navigator.userAgent.toLowerCase();
	this.data.browser.ie = this.data.browser.name.indexOf("msie") > 0;
    //IE11
	this.data.browser.ie = this.data.browser.ie ? this.data.browser.ie : this.data.browser.name.search(/(msie\s|trident.*rv:)([\w.]+)/) != -1;
	this.data.browser.firefox = this.data.browser.name.indexOf("firefox") > 0;
    this.data.browser.chrome = this.data.browser.name.indexOf("chrome") > 0;    
	this.data.browser.chrome45 = false;
    this.data.browser.edge = this.data.browser.name.indexOf("Edge") > 0;
    this.data.browser.arm64 = this.data.browser.platform.indexOf("aarch64")>0;
    this.data.browser.mips64 = this.data.browser.platform.indexOf("mips64")>0;
	this.chrVer = navigator.appVersion.match(/Chrome\/(\d+)/);
	this.ffVer = this.data.browser.name.match(/Firefox\/(\d+)/);
	if (this.data.browser.edge) { this.data.browser.ie = this.data.browser.firefox = this.data.browser.chrome = this.data.browser.chrome45 = false; }

    this.initApp = function(){
        this.edgeApp = new WebServer(this);
        this.edgeApp.ent.on_close = function () { _this.pluginInited=false; };
        this.app = WordPasterApp;
        this.app.ins = this;
    };
    this.checkBrowser=function(){

        //Win64
        if (window.navigator.platform == "Win64")
        {
            $.extend(this.Config.ie,this.Config.ie64);
        }
        else if (this.data.browser.ie) {
    
        }//macOS
        else if (window.navigator.platform == "MacIntel") {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.ExePath = this.Config.mac.path;
        }
        else if (window.navigator.platform == "Linux x86_64") {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.ExePath = this.Config.linux.path;
        }
        else if (this.data.browser.arm64) {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.ExePath = this.Config.arm64.path;
        }
        else if (this.data.browser.mips64) {
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.Config.ExePath = this.Config.mips64.path;
        }//Firefox
        else if (this.data.browser.firefox)
        {
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
            this.data.browser.edge = true;
        } //chrome
        else if (this.data.browser.chrome)
        {
            _this.Config["XpiPath"] = _this.Config["CrxPath"];
            _this.Config["XpiType"] = _this.Config["CrxType"];
            
            this.data.browser.edge = true;
            this.app.postMessage = this.app.postMessageEdge;
            this.edgeApp.run = this.edgeApp.runChr;
        }
        else if (this.data.browser.edge)
        {
            this.app.postMessage = this.app.postMessageEdge;
        }
    };

    this.pluginLoad = function () {
        if (!this.pluginInited) {
            if (this.data.browser.edge) {
                this.edgeApp.connect();
            }
        }
    };
    this.pluginCheck = function () {
        if (!this.pluginInited) {
            this.setup_tip();
            this.pluginLoad();
            return false;
        }
        return true;
    };
    this.setup_tip = function () {
        if(0 != this.ui.dialog.paste) return;
        var dom = this.ui.setup.find("div").html("<div>控件加载中，如果未加载成功请先</div><span name='setup' class='btn'><img name='setup'/>安装控件</span><span name='setupOk' class='btn'><img name='ok'/>我已安装</span>");
        dom.find('span[name="setup"]').click(function () {
            window.open(_this.Config["ExePath"]);
        });
        dom.find(".btn").each(function () {
            $(this).hover(function () {
                $(this).addClass("btn-hover");
            }, function () {
                $(this).removeClass("btn-hover");
            });
        });
        dom.find('span[name="setupOk"]').click(function () {
            _this.pluginLoad();
        });
        dom.find("img[name='ok']").attr("src", pathRes + "ok.png");
        dom.find("img[name='setup']").attr("src", pathRes + "setup.png");
        this.ui.dialog.paste = layer.open({
            type: 1,
            title: "安装提示",
            closeBtn: 1,
            area: ['291px', '124px'],
            scrollbar: false,
            content: _this.ui.setup,
            end: function () {
                _this.ui.dialog.paste=0;
            }
        });
    };
    this.need_update = function ()
    {
        var dom = this.ui.setup.html("发现新版本，请<a name='w-exe' href='#' class='btn'>更新</a>");
        var lnk = dom.find('a[name="w-exe"]');
        lnk.attr("href", this.Config["ExePath"]);
        this.ui.dialog.paste = layer.open({
            type: 1,
            title: "更新提示",
            closeBtn: 1,
            area: ['170px', '113px'],
            content: _this.ui.setup,
            end: function () {
                _this.ui.dialog.paste=0;
            }
        });
    };
	this.setupTipClose = function ()
	{
	    var dom = this.ui.paste.msg.html("图片上传中......");
	    this.ui.paste.percent.show();
	    this.ui.paste.ico.show();
        this.CloseDialogPaste();
        this.setuped = true;
	};
	this.CheckUpdate = function ()
	{
	    if (this.Browser.CheckVer())
	    {
	        this.OpenDialogPaste();
	        var dom = this.ui.paste.msg.html("控件有新版本，请<a name='aCtl'>更新控件</a>");
	        var lnk = dom.find('a[name="aCtl"]');
	        lnk.attr("href", this.Config["ExePath"]);
	        this.ui.paste.percent.hide();
	        this.ui.paste.ico.hide();
	    }
	};

    //加载控件及HTML元素
	this.GetHtml = function ()
	{
	    //Word图片粘贴
	    var acx = "";
        //Word解析组件
        acx += ' <object name="' + this.iePasterName + '" classid="clsid:' + this.Config.ie.clsid + '"';
	    acx += ' codebase="' + this.Config.ie.path + '#version=' + this.Config["Version"] + '"';
	    acx += ' width="1" height="1" ></object>';
	    if (this.data.browser.edge) acx = '';
	    //单张图片上传窗口
	    acx += '<div name="imgPasterDlg" class="panel-paster" style="display:none;">';
	    acx += '<img name="ico" id="infIco" alt="进度图标"/><span name="msg">图片上传中...</span><span name="percent">10%</span>';
        acx += '</div>';
        //安装提示
        acx += '<div name="ui-setup" class="panel-paster panel-setup"><div style="padding:10px;"></div></div>';
	    //图片批量上传窗口
        acx += '<div name="filesPanel" class="panel-files" style="display: none;"></div>';
	    //
	    acx += '<div style="display: none;">';

	    //文件上传列表项模板
	    acx += '<div class="UploaderItem" name="fileItem" id="UploaderTemplate">\
		            <div class="UploaderItemLeft">\
		            <div name="fname" class="FileName top-space">HttpUploader程序开发.pdf</div>\
		            <div class="ProcessBorder top-space">\
		                <div name="process" class="Process"></div>\
		            </div>\
		            <div name="msg" class="PostInf top-space">已上传:15.3MB 速度:20KB/S 剩余时间:10:02:00</div>\
		        </div>\
		        <div class="UploaderItemRight">\
		            <a name="btn" class="Btn" href="javascript:void(0)">取消</a>\
		            <div name="percent" class="ProcessNum">35%</div>\
		        </div>';
	    acx += '</div>'; //template end
	    //分隔线
	    acx += '<div name="line" class="Line" id="FilePostLine"></div>';

	    //hide div end
	    acx += '</div>';
	    return acx;
	};

	this.LoadTo = function (o)
    {
        if (!WordPaster.inited)
        {
            var dom = o.append(this.GetHtml());
            this.ffPaster = dom.find('embed[name="' + this.ffPasterName + '"]').get(0);
            this.ieParser = dom.find('object[name="' + this.iePasterName + '"]').get(0);
            this.line = dom.find('div[name="line"]');
            this.fileItem = dom.find('div[name="fileItem"]');
            this.filesPanel = dom.find('div[name="filesPanel"]');
            this.imgUploaderDlg = dom.find('div[name="filesPanel"]');
            this.ui.paste.dlg = dom.find('div[name="imgPasterDlg"]');
            this.ui.single = dom.find('div[name="imgPasterDlg"]');
            this.ui.paste.ico = this.ui.paste.dlg.find('img[name="ico"]');
            this.ui.paste.ico.attr("src", this.ui.ico.upload);
            this.ui.paste.msg = this.ui.paste.dlg.find('span[name="msg"]');
            this.ui.paste.percent = this.ui.paste.dlg.find('span[name="percent"]');
            this.ui.setup = dom.find('div[name="ui-setup"]');

            this.init();
        }
        WordPaster.inited = true;
	};

    //在文档加载完毕后调用
	this.init = function ()
	{
        this.initApp();
        this.checkBrowser();
        if (!_this.data.browser.edge)
        {
            _this.parter = _this.ffPaster;
            if (_this.data.browser.ie) _this.parter = _this.ieParser;
            _this.parter.recvMessage = _this.recvMessage;
        }
        if (_this.data.browser.edge) {
            _this.edgeApp.connect();
        }
        else { _this.app.init(); }
	};

    //打开图片上传对话框
	this.OpenDialogFile = function ()
	{
        if(0 != this.ui.dialog.files) return;

        _this.imgUploaderDlg.show();
        this.ui.dialog.files = layer.open({
            type: 1,
            title: "图片上传窗口",
            closeBtn: 1,
            area: ['452px', '445px'],
            skin: 'layui-layer-nobg',
            shadeClose: false,
            content: _this.imgUploaderDlg,
            cancel: function () {
                if (_this.working) return false;
                return true;
            },
            end:function(){
                _this.ui.dialog.files=0;
                _this.imgUploaderDlg.hide();
            }
        });
	};
	this.CloseDialogFile = function ()
	{
        layer.closeAll();
	};

    //打开粘贴图片对话框
	this.OpenDialogPaste = function ()
	{
		if( 0 != this.ui.dialog.paste ) return;
        _this.ui.paste.dlg.show();
        this.ui.dialog.paste = layer.open({
            type: 1,
            title: "上传进度",
            closeBtn: 1,
            area: ['550px', '160px'],
            shadeClose: false,
            content: _this.ui.paste.dlg,
            cancel: function(){
        		if(_this.working)return false;
        		return true;
        	},
            end: function () {
                _this.ui.dialog.paste = 0;
                _this.ui.paste.dlg.hide();
            }        
        });
	};
	this.CloseDialogPaste = function ()
	{
        layer.closeAll();
	};
	this.InsertHtml = function (html)
	{
	    _this.Editor.execCommand("insertHtml", html);
	};
	this.GetEditor = function () { return this.Editor; };

    //在FCKeditor_OnComplete()中调用
	this.SetEditor = function (edt)
	{
	    _this.Editor = edt;
        return this;
	};

    //粘贴命令
	this.Paste = function (evt)
	{
	    this.PasteManual();
	};

    //手动粘贴
	this.PasteManual = function ()
	{
	    if( !this.pluginCheck() ) return;
	    if( this.working ) return;
		this.working=true;
		this.app.paste();
	};

    //powerpoint
    this.PastePPT = function () {
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.working = true;
        this.app.pastePPT();
    };
    this.importWord = function(){
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.working = true;
        this.app.importWord();        
    };
    this.importExcel = function(){
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.working = true;
        this.app.importExcel();        
    };

    //import word to img
    this.importWordToImg = function () {
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.working = true;
        this.app.importWordToImg();
    };

    //powerpoint
	this.importPPT = function ()
	{
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.working = true;
        this.app.importPPT();
	};

    //pdf
	this.ImportPDF = function ()
	{
        if (!this.pluginCheck()) return;
        if (this.working) return;
        this.app.importPDF();
	};

    //上传网络图片
	this.UploadNetImg = function ()
	{
		if( !this.pluginCheck() ) return;
	    var data = _this.Editor.getContent();
        this.app.pasteAuto(data);
	};

	/*
	根据ID删除上传任务
	参数:
	fileID
	*/
	this.Delete = function(fileID)
	{
		var obj = this.UploaderList[fileID];
		if (null == obj) return;

		var tbID = "item" + obj.FileID;
		var item = document.getElementById(tbID);
		if (item) document.removeChild(item); //删除
	};

	/*
		添加到上传列表
		参数
			index 上传对象唯一标识
			uploaderObj 上传对象
	*/
	this.AppendToUploaderList = function(index, uploaderObj)
	{
		this.UploaderList[index] = uploaderObj;
		++this.UploaderListCount;
	};

	/*
		添加到上传列表层
		参数
			fid 文件ID
			div 上传信息层对象
			obj 上传对象
	*/
	this.AppendToListDiv = function(fid, div, obj)
	{
		var line = this.line.clone(true); //分隔线
		line.attr("id", "FilePostLine" + fid)
            .css("display", "block");
		obj.Separator = line;

		this.filesPanel.append(div);
		this.filesPanel.append(line);
	};

	/*
		更新编辑器内容。
		在所有图片上传完后调用。
		在上传图片出现错误时调用。
	*/
	this.UpdateContent = function ()
	{
	    _this.InsertHtml(_this.EditorContent);
	};

	this.addImgLoc = function (img)
	{
	    var fid = img.id;
	    var task = new FileUploader(img.id, img.src, this, img.width, img.height);
	    this.fileMap[img.id] = task;//添加到文件映射表
	    var ui = this.fileItem.clone();
	    ui.css("display", "block").attr("id", "item" + fid);

	    var objFileName = ui.find('div[name="fname"]');
	    var divMsg = ui.find('div[name="msg"]');
	    var aBtn = ui.find('a[name="btn"]');
	    var divPercent = ui.find('div[name="percent"]');
	    var divProcess = ui.find('div[name="process"]');

	    objFileName.text(img.name).attr("title", img.name);
	    task.pProcess = divProcess;
	    task.pMsg = divMsg;
	    task.pMsg.text("");
	    task.pButton = aBtn;
	    aBtn.attr("fid", fid)
            .attr("domid", "item" + fid)
            .attr("lineid", "FilePostLine" + fid)
            .click(function ()
            {
                switch ($(this).text())
                {
                    case "暂停":
                    case "停止":
                        task.Stop();
                        break;
                    case "取消":
                        { task.Remove(); }
                        break;
                    case "续传":
                    case "重试":
                        task.Post();
                        break;
                }
            });
	    task.pPercent = divPercent;
	    task.pPercent.text("0%");
	    task.ImageTag = img.html; //图片标记
	    task.InfDiv = ui;//上传信息DIV

	    //添加到上传列表层
	    this.AppendToListDiv(fid, ui, task);

	    //添加到上传列表
	    this.AppendToUploaderList(fid, task);
	    task.Ready(); //准备
	    return task;
	};
	this.WordParser_PasteWord = function (json)
    {
	    this.postType = this.data.type.word;
	    this.EditorContent = json.word;
	    for (var i = 0, l = json.imgs.length; i < l; ++i)
	    {
	        this.addImgLoc(json.imgs[i]);
	    }
	};
	this.WordParser_PasteExcel = function (json)
	{
	    this.postType = this.data.type.word;
	    this.EditorContent = json.word;
	    for (var i = 0, l = json.imgs.length; i < l; ++i)
	    {
	        this.addImgLoc(json.imgs[i]);
	    }
	    this.OpenDialogFile();
	};
	this.WordParser_PasteHtml = function (json)
	{
	    this.postType = this.data.type.word;
	    this.InsertHtml(json.word);//
        this.CloseDialogFile();
	    this.working = false;
	};
	this.WordParser_PasteFiles = function (json)
	{
	    this.postType = this.data.type.local;
	    for (var i = 0, l = json.imgs.length; i < l; ++i)
	    {
	        var task = this.addImgLoc(json.imgs[i]);
	        task.PostLocalFile = true;//
	    }
	    this.OpenDialogFile();
	};
	this.WordParser_PasteImage = function (json)
	{
	    this.OpenDialogPaste();
	    this.ui.paste.msg.text("开始上传");
	    this.ui.paste.percent.text("1%");
	};
	this.WordParser_PasteAuto = function (json)
	{
	    this.postType = this.data.type.network;
	    for (var i = 0, l = json.imgs.length; i < l; ++i)
	    {
	        this.addImgLoc(json.imgs[i]);
	    }
	    this.OpenDialogFile();
	};
	this.WordParser_PostComplete = function (json)
	{
	    this.ui.paste.percent.text("100%");
	    this.ui.paste.msg.text("上传完成");
	    var img = "<img src=\"";
	    img += json.value;
	    img += "\" />";
	    this.InsertHtml(img);
	    this.CloseDialogPaste();
	    this.working = false;
	};
	this.WordParser_PostProcess = function (json)
	{
	    this.ui.paste.percent.text(json.percent);
	};
	this.WordParser_PostError = function (json)
	{
		this.OpenDialogPaste();
        this.ui.paste.msg.html(
            this.data.error[json.value] + "<br/>" +
            "PostUrl:" + this.Config["PostUrl"] + "<br/>" +
            "License2:" + this.Config["License2"] + "<br/>" +
            "当前url:" + window.location.href + "<br/>" +
            "环境：" + navigator.userAgent);
        this.ui.paste.ico.attr("src", this.ui.ico.error);
	    this.ui.paste.percent.text("");
	    this.working = false;
	};
	this.File_PostComplete = function (json)
	{
	    var up = this.fileMap[json.id];
	    up.postComplete(json);
	    delete up;//
	};
	this.File_PostProcess = function (json)
	{
	    var up = this.fileMap[json.id];
	    up.postProcess(json);
	};
	this.File_PostError = function (json)
	{
	    var up = this.fileMap[json.id];
	    up.postError(json);
	};
	this.Queue_Complete = function (json)
	{
	    //上传网络图片
	    if (_this.postType == this.data.type.network)
	    {
	        _this.GetEditor().setContent(json.word);
	    } //上传Word图片时才替换内容
	    else if (_this.postType == this.data.type.word)
	    {
	        _this.InsertHtml(json.word);//
            _this.event.dataReady(json);
	    }
	    this.CloseDialogFile();
	    _this.working = false;
	};
	this.load_complete_edge = function (json)
    {
		this.pluginInited = true;
        _this.app.init();
        this.CloseDialogPaste();
    };
    this.imgs_out_limit = function (json) {
        layer.alert(this.data.error["13"] + "<br/>文档图片数量：" + json.imgCount + "<br/>限制数量：" + json.imgLimit, { icon: 2 });
    };
    this.url_unauth = function (json) {
        layer.alert(this.data.error["9"] + "<br/>PostUrl：" + json.url, { icon: 2 });
    };
    this.state_change = function (json) {
        if (json.value == "parse_document")
        {
            this.OpenDialogFile();
            this.filesPanel.text("正在解析文档");
        }
        else if (json.value == "process_data") {
            this.filesPanel.text("正在处理数据");
        }
        else if (json.value == "export_image") {
            this.filesPanel.text("正在导出图片:"+json.index+"/"+json.count);
        }
        else if (json.value == "process_data_end")
        {
            this.filesPanel.text("");
        }
        else if (json.value == "parse_empty") {
            this.CloseDialogFile();
            _this.working = false;
        }
        else if (json.value == "feature_not_supported") {
	    layer.alert("此功能不支持", { icon: 2 });
            this.ui.paste.ico.attr("src",this.ui.ico.error);
            _this.working = false;
        }
    };
    this.load_complete = function (json)
    {
        if (this.websocketInited) return;
        this.websocketInited = true;
		this.pluginInited = true;

        var needUpdate = true;
        if (typeof (json.version) != "undefined")
        {
            this.setuped = true;
            if (json.version == this.Config.Version) {
                needUpdate = false;
            }
        }
        if (needUpdate) this.need_update();
    };
    this.recvMessage = function (msg)
	{
	    var json = JSON.parse(msg);
	    if      (json.name == "Parser_PasteWord") _this.WordParser_PasteWord(json);
	    else if (json.name == "Parser_PasteExcel") _this.WordParser_PasteExcel(json);
	    else if (json.name == "Parser_PasteHtml") _this.WordParser_PasteHtml(json);
	    else if (json.name == "Parser_PasteFiles") _this.WordParser_PasteFiles(json);
	    else if (json.name == "Parser_PasteImage") _this.WordParser_PasteImage(json);
	    else if (json.name == "Parser_PasteAuto") _this.WordParser_PasteAuto(json);
	    else if (json.name == "Parser_PostComplete") _this.WordParser_PostComplete(json);
	    else if (json.name == "Parser_PostProcess") _this.WordParser_PostProcess(json);
	    else if (json.name == "Parser_PostError") _this.WordParser_PostError(json);
	    else if (json.name == "File_PostProcess") _this.File_PostProcess(json);
	    else if (json.name == "File_PostComplete") _this.File_PostComplete(json);
	    else if (json.name == "File_PostError") _this.File_PostError(json);
        else if (json.name == "load_complete") _this.load_complete(json);
	    else if (json.name == "Queue_Complete") _this.Queue_Complete(json);
	    else if (json.name == "load_complete_edge") _this.load_complete_edge(json);
        else if (json.name == "state_change") _this.state_change(json);
        else if (json.name == "imgs_out_limit") _this.imgs_out_limit(json);
        else if (json.name == "url_unauth") _this.url_unauth(json);
    };

    this.loadScripts();
}

var WordPaster = {
    instance: null,
    inited:false,
    getInstance: function (cfg) {
        if (this.instance == null) {
            this.instance = new WordPasterManager(cfg);
            window.WordPaster = WordPaster;
        }
        return this.instance;
    }
}